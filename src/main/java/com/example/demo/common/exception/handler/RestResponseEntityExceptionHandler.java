package com.example.demo.common.exception.handler;


import com.example.demo.common.exception.PlantNotFoundException;
import com.example.demo.common.response.handle.Codes;
import com.example.demo.common.response.handle.Message;
import com.example.demo.common.response.handle.HttpResponse;
import com.example.demo.common.response.handle.Result;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler {

	@ExceptionHandler(value = PlantNotFoundException.class)
	protected ResponseEntity handlePlantNotFoundException(PlantNotFoundException ex) {
		return new HttpResponse<>(Result.error(Message.message(Codes.PLAT_NOT_FOUND, ex.getMessage())), HttpStatus.NOT_FOUND).get();
	}

	@ExceptionHandler(value = Exception.class)
	protected ResponseEntity handleException(Exception ex) {
		return new HttpResponse<>(Result.error(Message.message(Codes.GENERAL_ERROR, ex.getMessage() == null ? "A very very bad thing happened. We are working on it" : ex.getMessage())), HttpStatus.INTERNAL_SERVER_ERROR).get();
	}

}
