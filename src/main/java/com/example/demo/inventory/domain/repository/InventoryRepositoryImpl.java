package com.example.demo.inventory.domain.repository;

import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

public class InventoryRepositoryImpl implements CustomInventoryRepository {

    @Autowired
    EntityManager em;

    public List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("SELECT p.plantInfo FROM PlantInventoryItem p WHERE LOWER(p.plantInfo.name) like LOWER(concat('%', ?1, '%')) and p not in " +
                        "(SELECT r.plant FROM PlantReservation r WHERE ?2 < r.schedule.endDate AND ?3 > r.schedule.startDate)",
                PlantInventoryEntry.class)
                .setParameter(1, name)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }

    public List<PlantInventoryItem> findAvailableItems(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery("select p from PlantInventoryItem p where LOWER(p.plantInfo.name) like ?1 and p not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.schedule.endDate and ?3 > r.schedule.startDate)",
                PlantInventoryItem.class)
                .setParameter(1, name)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }

    @Override
    public Boolean doesPlantInventoryItemExists(Long plantInvItemId) {
        return em.createQuery("SELECT CASE WHEN (COUNT (item) > 0)  THEN TRUE ELSE FALSE END FROM PlantInventoryItem item WHERE item.id = ?1",
                Boolean.class).setParameter(1, plantInvItemId).getSingleResult();
    }

    @Override
    public Boolean doesPlantInventoryEntryExists(Long plantInvEntId) {
        return em.createQuery("SELECT CASE WHEN (COUNT (entry) > 0)  THEN TRUE ELSE FALSE END FROM PlantInventoryEntry entry WHERE entry.id = ?1",
                Boolean.class).setParameter(1, plantInvEntId).getSingleResult();
    }

}

