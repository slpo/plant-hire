package com.example.demo.inventory.application.dto;

import com.example.demo.inventory.domain.model.PlantInventoryItem;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

@Data
public class PlantInventoryEntryAndItemsDTO extends ResourceSupport {

	PlantInventoryEntryDTO entryDTO;
	List<PlantInventoryItem> inventoryItems;
}
