package com.example.demo.common.response.handle;

public final class Codes {
	
	private Codes(){
		
	}
	
	public static final String OBJECT_IS_NULL = "err.objectIsNull";
	public static final String VALUE_IS_NULL = "err.valueIsNull";
	public static final String VALUE_IS_NOT_VALID = "err.valueIsNotValid";

	public static final String NO_PLANT_ERROR = "err.noPlantAvailable";
	public static final String PLAT_NOT_FOUND = "err.plantNotFound";
	public static final String GENERAL_ERROR = "err.generalError";

}
