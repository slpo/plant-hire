package com.example.demo.common.response.handle;


import com.sun.deploy.util.StringUtils;

import java.util.Collection;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

public class Message {
	private String code;
	private String mess;

	private Message(String code, String mess) {
		this.code = code;
		this.mess = mess;
	}

	public Message(String code) {
		this.code = code;
	}

	public static Message message(String code, String mess) {
		return new Message(code, mess);
	}

	public static Message message(String code) {
		return new Message(code);
	}

	public String getCode() {
		return code;
	}
	public String getMess() {
		return mess;
	}
	public static String concatErrors(Collection<Message> errors) {
		return StringUtils.join(errors.stream().map(Message::getCode).collect(toList()), ",");
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Message message = (Message) o;

		return Objects.equals(code, message.code);
	}

	@Override
	public int hashCode() {
		return code != null ? code.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "Message{" +
				"code='" + code + '\'' +
				", Mess='" + mess + '\'' +
				'}';
	}

}

