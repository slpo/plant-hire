package com.example.demo.sales.domain.repository;

import com.example.demo.sales.domain.model.POExtension;
import org.springframework.data.jpa.repository.JpaRepository;

public interface POExtensionRepo extends JpaRepository<POExtension,Long> {
}
