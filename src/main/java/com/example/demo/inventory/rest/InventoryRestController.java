package com.example.demo.inventory.rest;

import com.example.demo.common.exception.PlantNotFoundException;
import com.example.demo.common.response.handle.Response;
import com.example.demo.inventory.application.dto.PlantInventoryEntryAndItemsDTO;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.application.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin
@RequestMapping("api/plants")
public class InventoryRestController {

    @Autowired
    InventoryService service;

    @PostMapping
    public Response<PlantInventoryEntryDTO> create(@RequestBody PlantInventoryEntryDTO entryDTO) {
        return service.create(entryDTO);
    }

    @PostMapping("/equipment")
    public Response<PlantInventoryItemDTO> addPlantInventory(@RequestBody PlantInventoryItemDTO dto) throws PlantNotFoundException {
        return service.addEquipment(dto);
    }

    @GetMapping
    public Response<List<PlantInventoryEntryDTO>> getAll() {
        return service.getAllPlants();
    }

    
    @GetMapping("/{id}")
    public Response<PlantInventoryEntryDTO> getPlant(@PathVariable("id") Long id) throws PlantNotFoundException {
        return service.findPlant(id);
    }

    @GetMapping("/{id}/details")
    public Response<PlantInventoryEntryAndItemsDTO> getPlantDetail(@PathVariable("id") Long id) throws PlantNotFoundException {
        return service.getPlantDetails(id);
    }



}
