package com.example.demo.sales.domain.validator;


import com.example.demo.common.response.handle.Codes;
import com.example.demo.sales.domain.model.POStatus;
import com.example.demo.sales.domain.model.PurchaseOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class UpdatePurchaseOrderValidator implements Validator {

	private String type;

	public static final String PREALLOCATE = "preallocate";
	public static final String ACCEPT = "accept";
	public static final String REJECT = "reject";
	public static final String CANCEL = "cancel";
	public static final String DISPATCH = "dispatch";
	public static final String EXTENSION = "extension";
	public static final String NEW = "new";

	private static Logger logger = LoggerFactory.getLogger(UpdatePurchaseOrderValidator.class);

	public UpdatePurchaseOrderValidator(String type) {
		this.type = type;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(PurchaseOrder.class);
	}

	@Override
	public void validate(Object obj, Errors errors) {

		PurchaseOrder po = (PurchaseOrder) obj;
		LocalDate startDate = po.getRentalPeriod().getStartDate();
		LocalDate now = LocalDate.now();
		long daysBetween = ChronoUnit.DAYS.between(now, startDate);
		switch (type) {
			case ACCEPT:
			case REJECT:
				if (po.getStatus() != POStatus.PENDING) {
					errors.rejectValue("status", Codes.VALUE_IS_NOT_VALID, "Purchase order status is not Pending");
				}
				break;
			case DISPATCH:
				if (po.getStatus() != POStatus.ACCEPTED) {
					errors.rejectValue("status", Codes.VALUE_IS_NOT_VALID, "Purchase order has not been accepted");
				}

				if (daysBetween != 0) {
					errors.rejectValue("status", Codes.VALUE_IS_NOT_VALID, "Delivery date is not now");
				}
				break;
			case CANCEL:
				if (po.getStatus() == POStatus.PLANT_DISPATCHED || po.getStatus() == POStatus.PLANT_RETURNED || po.getStatus() == POStatus.INVOICED || daysBetween <= 1) {
					errors.rejectValue("status", Codes.VALUE_IS_NOT_VALID, "We can not cancel this PO");
				}
				break;
			case EXTENSION:
				logger.info("Status = {}",po.getStatus());
				if (po.getStatus() == POStatus.PENDING || po.getStatus() == POStatus.PLANT_RETURNED || po.getStatus() == POStatus.INVOICED) {
					errors.rejectValue("status", Codes.VALUE_IS_NOT_VALID, "PO can not be extended");
				}
				break;
		}

	}
}
