package com.example.demo.sales.domain.validator;

import com.example.demo.common.application.sevice.BusinessPeriodValidator;
import com.example.demo.common.response.handle.Codes;
import com.example.demo.sales.domain.model.PurchaseOrder;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.LocalDate;

import static java.util.Objects.isNull;

public class PurchaseOrderValidator implements Validator {

	@Override
	public boolean supports(@NonNull Class clazz) {
		return PurchaseOrder.class.equals(clazz);
	}

	@Override
	public void validate(@Nullable Object obj, @NonNull Errors errors) {
		if (isNull(obj)) {
			errors.reject(Codes.OBJECT_IS_NULL, "Purchase order is null");
			return;
		}

		PurchaseOrder po = (PurchaseOrder) obj;

		if (isNull(po.getPlant())) {
			errors.rejectValue("plant", Codes.OBJECT_IS_NULL, "Plant for purchase order is null");
		}

		if (isNull(po.getStatus())) {
			errors.rejectValue("status", Codes.VALUE_IS_NULL, "Status of purchase order is null");
		}

		switch (po.getStatus()) {
			case PENDING:
				if (!isNull(po.getRentalPeriod())
						&& !isNull(po.getRentalPeriod().getStartDate())
						&& po.getRentalPeriod().getStartDate().isBefore(LocalDate.now())) {
					errors.rejectValue("rentalPeriod.startDate", Codes.VALUE_IS_NOT_VALID , "Start date of purchase order is in the past");
				}
				break;
			case ACCEPTED:
				if (isNull(po.getTotal())) {
					errors.rejectValue("total", Codes.VALUE_IS_NULL, "Purchase order is in state " + po.getStatus() + " but total is null");
				}
			default:
				if (isNull(po.getId())) {
					errors.rejectValue("id", Codes.VALUE_IS_NULL, "Purchase ID is null");
				}
		}

		if (!isNull(po.getTotal()) && po.getTotal().signum() < 1) {
			errors.rejectValue("total", Codes.VALUE_IS_NOT_VALID, "Purchase order total is less than one");
		}

		try {
			errors.pushNestedPath("rentalPeriod");
			ValidationUtils.invokeValidator(new BusinessPeriodValidator(), po.getRentalPeriod(), errors);
		} finally {
			errors.popNestedPath();
		}


	}

}
