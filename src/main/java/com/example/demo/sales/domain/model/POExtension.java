package com.example.demo.sales.domain.model;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class POExtension {

	@Id
	@GeneratedValue
	Long id;

	LocalDate endDate;

	@Enumerated(EnumType.STRING)
	POExtensionStatus status;


	public POExtension(LocalDate endDate, POExtensionStatus status) {
		this.endDate = endDate;
		this.status = status;
	}
}
