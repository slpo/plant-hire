package com.example.demo.inventory.application.service;


import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.rest.InventoryRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

@Service
public class PlantInvItemAssembler extends ResourceAssemblerSupport<PlantInventoryItem, PlantInventoryItemDTO> {

	public PlantInvItemAssembler() {
		super(InventoryRestController.class, PlantInventoryItemDTO.class);
	}

	@Override
	public PlantInventoryItemDTO toResource(PlantInventoryItem entity) {
		PlantInventoryItemDTO dto = new PlantInventoryItemDTO();
		dto.set_id(entity.getId());
		dto.setEquipmentCondition(entity.getEquipmentCondition());
		dto.setSerialNumber(entity.getSerialNumber());
		return dto;
	}
}
