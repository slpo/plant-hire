package com.example.demo.sales.rest;

import com.example.demo.common.exception.PlantNotFoundException;
import com.example.demo.common.response.handle.ConstructResponse;
import com.example.demo.common.response.handle.Response;
import com.example.demo.inventory.application.service.InventoryService;
import com.example.demo.sales.application.dto.POExtensionDTO;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.application.service.SalesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.time.LocalDate;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@CrossOrigin
@RestController
@RequestMapping("/api/sales")
public class SalesRestController extends ConstructResponse {
    @Autowired
    InventoryService inventoryService;

    @Autowired
    SalesService salesService;

    private static final Logger logger = LoggerFactory.getLogger(SalesRestController.class);

    @GetMapping("/plants")
    public ResponseEntity findAvailablePlants(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        Response plantInventoryEntryDTOS = inventoryService.findAvailablePlants(name, startDate, endDate);
        logger.info("Returning available plants {}", plantInventoryEntryDTOS);
        return createResponse(plantInventoryEntryDTOS);
    }


    @PostMapping("/orders")
    public ResponseEntity createPurchaseOrder(@RequestBody PurchaseOrderDTO partialPODTO) {
        try {
            if (!inventoryService.doesPlantInventoryEntryExists(partialPODTO)) {
                throw new PlantNotFoundException(partialPODTO.getPlant().get_id());
            }


            Response purchaseOrderDTO = salesService.createPO(partialPODTO);
            if (!purchaseOrderDTO.wasSuccessful()) {
                return createResponse(purchaseOrderDTO, HttpStatus.BAD_REQUEST);
            }
            PurchaseOrderDTO createdPurchaseOrder = (PurchaseOrderDTO) purchaseOrderDTO.getData();


            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(new URI(createdPurchaseOrder.getLink().getHref()));
            return new ResponseEntity<>(
                    new Resource<>(createdPurchaseOrder,
                            linkTo(methodOn(SalesRestController.class).
                                    fetchPurchaseOrder(createdPurchaseOrder.get_id())).withSelfRel()),
                    headers,
                    HttpStatus.CREATED);
        }catch (Exception e){
            e.printStackTrace();
        }
        return createResponse(null);
    }



    @GetMapping("/orders")
    public ResponseEntity getAllPendingOrders() {
        Response<List<PurchaseOrderDTO>> purchaseOrderDTOS = salesService.getOrders();
        logger.debug("Pending Purchase Orders {}", purchaseOrderDTOS);
        return createResponse(purchaseOrderDTOS);
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Response<PurchaseOrderDTO>> fetchPurchaseOrder(@PathVariable("id") Long id) {
        Response<PurchaseOrderDTO> purchaseOrder = salesService.findPO(id);
        logger.debug("Purchase Order {}", purchaseOrder);
        return createResponse(purchaseOrder);
    }


    @PostMapping("orders/status")
    public List<PurchaseOrderDTO> getPOStatuses(@RequestBody List<Long> ids) {
        return salesService.getPOStatuses(ids);
    }

    @PutMapping("orders/{id}/accept")
    public ResponseEntity acceptPurchaseOrder(@PathVariable Long id) {
        return createResponse(salesService.acceptPO(id));
    }

    @PutMapping("orders/{id}/cancel")
    public ResponseEntity cancelPO(@PathVariable Long id) {
        return createResponse(salesService.cancelPO(id));
    }

    @PutMapping("orders/{id}/reject")
    public ResponseEntity rejectPurchaseOrder(@PathVariable Long id) throws Exception {
        return createResponse(salesService.rejectPO(id));
    }

    @PutMapping("orders/{id}/dispatch")
    public ResponseEntity dispatchPurchaseOrder(@PathVariable Long id) throws Exception {
        return createResponse(salesService.dispatchPO(id));
    }

    @PutMapping("orders/{id}/extend")
    public ResponseEntity requestExtension(@PathVariable Long id, @RequestBody POExtensionDTO poExtensionDTO) {
        return createResponse(salesService.addExtension(id, poExtensionDTO));
    }

    @PutMapping("orders/{id}/extension/accept")
    public ResponseEntity acceptExtension(@PathVariable Long id) throws Exception {
        return createResponse(salesService.acceptExtension(id));
    }


    @PutMapping("orders/{id}/extension/reject")
    public ResponseEntity rejectExtension(@PathVariable Long id) throws Exception {
        return createResponse(salesService.rejectExtension(id));
    }



}