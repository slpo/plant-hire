package com.example.demo.sales.application.dto;

import com.example.demo.sales.domain.model.POExtensionStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;

@Data
public class POExtensionDTO {

	Long _id;

	@JsonFormat(pattern = "yyyy-MM-dd")
	LocalDate endDate;

	POExtensionStatus status;

	public POExtensionDTO(Long _id, LocalDate endDate, POExtensionStatus status) {
		this._id = _id;
		this.endDate = endDate;
		this.status = status;
	}

	public POExtensionDTO() {
	}

	public Long get_id() {
		return _id;
	}

	public void set_id(Long _id) {
		this._id = _id;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public POExtensionStatus getStatus() {
		return status;
	}

	public void setStatus(POExtensionStatus status) {
		this.status = status;
	}
}
