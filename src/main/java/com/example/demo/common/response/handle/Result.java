package com.example.demo.common.response.handle;

import java.util.Arrays;
import java.util.Collection;

public class Result {

	private Result() {
	}

	public static <T> Response<T> error(Message... messages) {
		return new Response<>(Arrays.asList(messages));
	}

	public static <T> Response<T> error(Collection<Message> messages) {
		return new Response<>(messages);
	}

	public static <T> Response<T> success(T data) {
		return new Response<>(data);
	}
}
