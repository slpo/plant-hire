package com.example.demo.sales.domain.repository;

import com.example.demo.sales.domain.model.PurchaseOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long>,CustomPurchaseOrderRepo {
    List<PurchaseOrder> findAllByIdIn(List<Long> id);
}
