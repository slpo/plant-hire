package com.example.demo.common.response.handle;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class ConstructResponse {

	protected static <T extends Response> ResponseEntity<T> createResponse(T result) {
		return new HttpResponse<>(result).get();
	}

	protected static <T extends Response> ResponseEntity<T> createResponse(T result, HttpStatus status) {
		return new HttpResponse<>(result, status).get();
	}
}
