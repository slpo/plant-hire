# ESI practical

Prject to practice Domain Driven Design. Implements the scinario descriped [here](https://courses.cs.ut.ee/MTAT.03.229/2019_spring/uploads/Main/PlantHireScenario.pdf)

Based on the course [Enterprise System Integaration](https://courses.cs.ut.ee/2019/esi/spring/Main/HomePage)

University of Tartu. Master of Software Engineering.