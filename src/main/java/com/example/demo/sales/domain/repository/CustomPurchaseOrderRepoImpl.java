package com.example.demo.sales.domain.repository;


import com.example.demo.sales.domain.model.POStatus;
import com.example.demo.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.List;

public class CustomPurchaseOrderRepoImpl implements CustomPurchaseOrderRepo{

	@Autowired
    EntityManager em;

	@Override
	public List<PurchaseOrder> findPurchaseWithStatus(POStatus status) {
		return em.createQuery("SELECT po FROM PurchaseOrder po WHERE po.status = ?1"
				, PurchaseOrder.class)
				.setParameter(1, status)
				.getResultList();
	}
}
