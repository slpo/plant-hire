package com.example.demo.sales.domain.model;

import com.example.demo.common.domain.model.BusinessPeriod;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantReservation;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class PurchaseOrder {
    @Id
    @GeneratedValue
    Long id;

    @OneToMany
    List<PlantReservation> reservations;

    @OneToMany
    List<POExtension> extensions = new ArrayList<>();

    @ManyToOne
    PlantInventoryEntry plant;

    LocalDate issueDate;

    @Column(precision=8,scale=2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;

    @Embedded
    BusinessPeriod rentalPeriod;

    String deliveryAddress;

    public static PurchaseOrder of(PlantInventoryEntry entry, BusinessPeriod period, String address) {
        PurchaseOrder po = new PurchaseOrder();
        po.plant = entry;
        po.rentalPeriod = period;
        po.reservations = new ArrayList<>();
        po.issueDate = LocalDate.now();
        po.status = POStatus.PENDING;
        po.deliveryAddress = address;
        return po;
    }

    public void setStatus(POStatus newStatus) {
        status = newStatus;
    }

    public void addReservation(PlantReservation reservation) {
        reservations.add(reservation);
    }
}