package com.example.demo.inventory.domain.model;

public enum EquipmenetCondition {
    SERVICEABLE, UNSERVICEABLE
}
