package com.example.demo.sales.domain.repository;


import com.example.demo.sales.domain.model.POStatus;
import com.example.demo.sales.domain.model.PurchaseOrder;

import java.util.List;

public interface CustomPurchaseOrderRepo {

	List<PurchaseOrder> findPurchaseWithStatus(POStatus status);

}
