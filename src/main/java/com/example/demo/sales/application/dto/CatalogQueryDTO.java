package com.example.demo.sales.application.dto;

import lombok.Data;
import com.example.demo.common.application.dto.BusinessPeriodDTO;

@Data
public class CatalogQueryDTO {
    String name;
    BusinessPeriodDTO rentalPeriod;
}
