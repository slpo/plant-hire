package com.example.demo.sales.application.service;

import com.example.demo.common.response.handle.Codes;
import com.example.demo.common.domain.model.BusinessPeriod;
import com.example.demo.common.response.handle.Message;
import com.example.demo.common.response.handle.Response;
import com.example.demo.common.response.handle.Result;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.model.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.POExtensionDTO;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.domain.model.POExtension;
import com.example.demo.sales.domain.model.POExtensionStatus;
import com.example.demo.sales.domain.model.POStatus;
import com.example.demo.sales.domain.model.PurchaseOrder;
import com.example.demo.sales.domain.repository.POExtensionRepo;
import com.example.demo.sales.domain.repository.PurchaseOrderRepository;
import com.example.demo.sales.domain.validator.PurchaseOrderValidator;
import com.example.demo.sales.domain.validator.UpdatePurchaseOrderValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SalesService {

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    @Autowired
    PurchaseOrderRepository poRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    POExtensionRepo poExtensionRepo;

    private static final Logger logger = LoggerFactory.getLogger(SalesService.class);

    public Response<PurchaseOrderDTO> findPO(Long id) {
        PurchaseOrder po = getPO(id);
        if (po == null) {
            return Result.error(new Message(Codes.OBJECT_IS_NULL));
        }
        return Result.success(purchaseOrderAssembler.toResource(po));
    }

    public Response createPO(PurchaseOrderDTO poDTO) {

        BusinessPeriod period = BusinessPeriod.of(
                poDTO.getRentalPeriod().getStartDate(),
                poDTO.getRentalPeriod().getEndDate());

        PlantInventoryEntry plant = plantInventoryEntryRepository.findById(poDTO.getPlant().get_id()).orElse(null);

        PurchaseOrder po = PurchaseOrder.of(plant, period, poDTO.getAddress());


        logger.info("Validating purchase order {}", po);

        DataBinder binder = new DataBinder(po);
        binder.addValidators(new PurchaseOrderValidator());
        Response errorMessages = validatePO(binder);
        if (errorMessages != null) return errorMessages;

        poRepository.save(po);

        List<PlantInventoryItem> availableItems = inventoryRepository.findAvailableItems(
                po.getPlant().getName().toLowerCase(),
                po.getRentalPeriod().getStartDate(),
                po.getRentalPeriod().getEndDate());

        if (availableItems.isEmpty()) {
            logger.info("No plant available for purchase order {}. Rejecting ...", po);
            po.setStatus(POStatus.REJECTED);
            poRepository.save(po);
            return Result.error(Message.message(Codes.NO_PLANT_ERROR, "No plant available for the purchase"));
        }
        PlantReservation reservation = new PlantReservation();
        reservation.setSchedule(po.getRentalPeriod());
        reservation.setPlant(availableItems.get(0));

        plantReservationRepository.save(reservation);
        po.getReservations().add(reservation);
        po.setStatus(POStatus.PENDING);
        poRepository.save(po);
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderAssembler.toResource(po);
        logger.info("Newly created purchase order {}", purchaseOrderDTO);
        return Result.success(purchaseOrderDTO);
    }


    public Response acceptPO(Long id) {
        PurchaseOrder purchaseOrder = getPO(id);
        Response errorMessages = this.validatePO(purchaseOrder, UpdatePurchaseOrderValidator.ACCEPT);
        if (errorMessages != null) return errorMessages;

        logger.info("Updating PO{} status to {}", purchaseOrder, POStatus.ACCEPTED);
        purchaseOrder.setStatus(POStatus.ACCEPTED);
        poRepository.save(purchaseOrder);

        return Result.success(purchaseOrderAssembler.toResource(purchaseOrder));
    }

    public Response cancelPO(Long id) {
        PurchaseOrder purchaseOrder = getPO(id);
        Response errorMessages = this.validatePO(purchaseOrder, UpdatePurchaseOrderValidator.CANCEL);
        if (errorMessages != null) {
            return errorMessages;
        }
        logger.info("Updating PO{} status to {}", purchaseOrder, POStatus.CANCELLED);
        purchaseOrder.setStatus(POStatus.CANCELLED);
        poRepository.save(purchaseOrder);

        return Result.success(purchaseOrderAssembler.toResource(purchaseOrder));
    }

    private Response validatePO(DataBinder binder) {
        binder.validate();

        if (binder.getBindingResult().hasErrors()) {
            List<ObjectError> errors = binder.getBindingResult().getAllErrors();

            List<Message> errorMessages = errors.stream().map(error -> Message.message(((FieldError) error).getField(), error.getDefaultMessage())).collect(Collectors.toList());
            logger.info("Validation errors {}", errorMessages);
            return Result.error(errorMessages);
        }
        return null;
    }

    public Response rejectPO(Long id) throws Exception {
        PurchaseOrder purchaseOrder = getPO(id);
        Response errorMessages = this.validatePO(purchaseOrder, UpdatePurchaseOrderValidator.REJECT);
        if (errorMessages != null) return errorMessages;
        while (!purchaseOrder.getReservations().isEmpty()) {
            plantReservationRepository.delete(purchaseOrder.getReservations().remove(0));
        }
        purchaseOrder.setStatus(POStatus.REJECTED);
        poRepository.save(purchaseOrder);

        return Result.success(purchaseOrderAssembler.toResource(purchaseOrder));
    }

    public Response dispatchPO(Long id) throws Exception {
        PurchaseOrder purchaseOrder = getPO(id);
        Response errorMessages = this.validatePO(purchaseOrder, UpdatePurchaseOrderValidator.DISPATCH);
        if (errorMessages != null) return errorMessages;
        purchaseOrder.setStatus(POStatus.PLANT_DISPATCHED);
        poRepository.save(purchaseOrder);

        return Result.success(purchaseOrderAssembler.toResource(purchaseOrder));
    }

    public Response addExtension(Long id, POExtensionDTO poExtensionDTO) {
        try {
            PurchaseOrder purchaseOrder = getPO(id);
            List<POExtension> extensions = purchaseOrder.getExtensions().stream().filter(extension -> extension.getStatus() == POExtensionStatus.PENDING).collect(Collectors.toList());
            if (!extensions.isEmpty()) {
                return Result.error(Message.message("Pending extension"));
            }
            Response errorMessages = this.validatePO(purchaseOrder, UpdatePurchaseOrderValidator.EXTENSION);
            if (errorMessages != null) return errorMessages;
            POExtension poExtension = poExtensionRepo.save(new POExtension(poExtensionDTO.getEndDate(), POExtensionStatus.PENDING));
            purchaseOrder.getExtensions().add(poExtension);
            purchaseOrder.setStatus(POStatus.EXTENSION_REQUESTED);
            poRepository.save(purchaseOrder);
            PurchaseOrderDTO purchaseOrderDTO = purchaseOrderAssembler.toResource(purchaseOrder);
            logger.info("PDTO {}",purchaseOrderDTO);
            return Result.success(purchaseOrderDTO);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return Result.error(Message.message(""));
    }

    public Response acceptExtension(Long id) {
        PurchaseOrder purchaseOrder = getPO(id);
        List<POExtension> extensions = purchaseOrder.getExtensions().stream().filter(extension -> extension.getStatus() == POExtensionStatus.PENDING).collect(Collectors.toList());
        if (extensions.isEmpty()) {
            return Result.error(Message.message("No Pending extension"));
        }
        POExtension extension = extensions.get(0);
        extension.setStatus(POExtensionStatus.ACCEPTED);
        poExtensionRepo.save(extension);
        purchaseOrder.setStatus(POStatus.EXTENSION_ACCEPTED);



        purchaseOrder.setRentalPeriod(BusinessPeriod.of(purchaseOrder.getRentalPeriod().getStartDate(),extension.getEndDate()));

        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderAssembler.toResource(purchaseOrder);
        logger.info("PDTO {}",purchaseOrderDTO);
        return Result.success(purchaseOrderDTO);
    }


    public Response rejectExtension(Long id) {
        PurchaseOrder purchaseOrder = getPO(id);
        List<POExtension> extensions = purchaseOrder.getExtensions().stream().filter(extension -> extension.getStatus() == POExtensionStatus.PENDING).collect(Collectors.toList());
        if (extensions.isEmpty()) {
            return Result.error(Message.message("No Pending extension"));
        }
        POExtension extension = extensions.get(0);
        extension.setStatus(POExtensionStatus.REJECTED);
        purchaseOrder.setStatus(POStatus.EXTENSION_REJECTED);
        poRepository.save(purchaseOrder);
        poExtensionRepo.save(extension);
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderAssembler.toResource(purchaseOrder);
        logger.info("PDTO {}",purchaseOrderDTO);
        return Result.success(purchaseOrderDTO);
    }

    private Response validatePO(PurchaseOrder purchaseOrder, String type) {
        logger.info("Type = {}", type);
        DataBinder binder = new DataBinder(purchaseOrder);
        binder.addValidators(new UpdatePurchaseOrderValidator(type));
        return validatePO(binder);
    }

    private PurchaseOrder getPO(Long id) {
        logger.info("Finding purchase order with ID {}", id);
        return poRepository.findById(id).orElse(null);
    }

    public Response<List<PurchaseOrderDTO>> getOrders() {
        logger.info("Finding all pending purchase orders");
        List<PurchaseOrderDTO> purchaseOrders = poRepository.findAll().stream().map(po -> purchaseOrderAssembler.toResource(po)).collect(Collectors.toList());
        return Result.success(purchaseOrders);
    }

    public List<PurchaseOrderDTO> getPOStatuses(List<Long> ids) {
        return poRepository.findAllByIdIn(ids).stream().map((po -> purchaseOrderAssembler.toResource(po))).collect(Collectors.toList());
    }

}
