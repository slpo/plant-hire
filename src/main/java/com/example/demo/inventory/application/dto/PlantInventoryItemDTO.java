package com.example.demo.inventory.application.dto;

import com.example.demo.inventory.domain.model.EquipmenetCondition;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;

@Data
public class PlantInventoryItemDTO extends ResourceSupport {
    Long _id;
    String serialNumber;
    EquipmenetCondition equipmentCondition;
    PlantInventoryEntryDTO plantInfo;
}
