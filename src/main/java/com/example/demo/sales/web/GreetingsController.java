package com.example.demo.sales.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GreetingsController {

    @GetMapping
    public String welcome(){

        return "welcome";

    }
}
