package com.example.demo.inventory.application.service;

import com.example.demo.common.exception.PlantNotFoundException;
import com.example.demo.common.response.handle.Response;
import com.example.demo.common.response.handle.Result;
import com.example.demo.inventory.application.dto.PlantInventoryEntryAndItemsDTO;
import com.example.demo.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.demo.inventory.application.dto.PlantInventoryItemDTO;
import com.example.demo.inventory.domain.model.PlantInventoryEntry;
import com.example.demo.inventory.domain.model.PlantInventoryItem;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryItemRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Service
public class InventoryService {

    private static final Logger logger = LoggerFactory.getLogger(InventoryService.class);

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepository;

    @Autowired
    PlantInvItemAssembler plantInvItemAssembler;

    public Response<PlantInventoryEntryDTO> create(PlantInventoryEntryDTO entryDTO) {
        logger.info("Creating plant entry {}", entryDTO);
        PlantInventoryEntry entry = new PlantInventoryEntry();
        entry.setDescription(entryDTO.getDescription());
        entry.setName(entryDTO.getName());
        entry.setPrice(entryDTO.getPrice());
        return Result.success(plantInventoryEntryAssembler.toResource(inventoryRepository.save(entry)));
    }

    public Response<PlantInventoryItemDTO> addEquipment(PlantInventoryItemDTO dto) throws PlantNotFoundException {
        logger.info("Creating a new equipment {}",dto);
        PlantInventoryEntry entry = inventoryRepository.findById(dto.getPlantInfo().get_id()).orElseThrow(() -> new PlantNotFoundException(dto.getPlantInfo().get_id()));
        PlantInventoryItem item = new PlantInventoryItem();
        item.setEquipmentCondition(dto.getEquipmentCondition());
        item.setPlantInfo(entry);
        item.setSerialNumber(dto.getSerialNumber());
        return Result.success(plantInvItemAssembler.toResource(plantInventoryItemRepository.save(item)));
    }


    public Response<List<PlantInventoryEntryDTO>> getAllPlants() {
        logger.info("Getting all plants");
        List<PlantInventoryEntry> entries = inventoryRepository.findAll();
        return Result.success(plantInventoryEntryAssembler.toResources(entries));
    }

    public Response<List<PlantInventoryEntryDTO>> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        logger.info("Finding available plants from {} to {} and with name {}", startDate, endDate, name);
        List<PlantInventoryEntry> entries = inventoryRepository.findAvailablePlants(name, startDate, endDate);
        return Result.success(plantInventoryEntryAssembler.toResources(entries));
    }

    public Response<PlantInventoryEntryDTO> findPlant(Long id) throws PlantNotFoundException {
        return Result.success(plantInventoryEntryAssembler.toResource(inventoryRepository.findById(id).orElseThrow(()-> new PlantNotFoundException(id))));
    }


    public Response<PlantInventoryEntryAndItemsDTO> getPlantDetails(Long id) throws PlantNotFoundException {
        logger.info("Find plant with id {}", id);
        PlantInventoryEntry plantInventoryEntry = inventoryRepository.findById(id).orElseThrow(() -> new PlantNotFoundException(id));
        List<PlantInventoryItem> items = plantInventoryItemRepository.findAllByPlantInfo(plantInventoryEntry);
        PlantInventoryEntryAndItemsDTO response = new PlantInventoryEntryAndItemsDTO();
        response.setEntryDTO(plantInventoryEntryAssembler.toResource(plantInventoryEntry));
        response.setInventoryItems(items);
        return Result.success(response);
    }

    public Boolean doesPlantInventoryItemExists(Long id) {
        return inventoryRepository.doesPlantInventoryItemExists(id);
    }

    public Boolean doesPlantInventoryEntryExists(PurchaseOrderDTO purchaseOrderDTO) {
        if (Objects.isNull(purchaseOrderDTO) || Objects.isNull(purchaseOrderDTO.getPlant())) {
            return false;
        }
        return inventoryRepository.doesPlantInventoryEntryExists(purchaseOrderDTO.getPlant().get_id());
    }
}
